package com.example.myapplication.assignment1

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val s = scanner.nextLine()
    var count = 1
    for (i in s.indices) {
        if (s[i] == ' ') {
            if (s[i + 1] in 'a'..'z' || s[i + 1] in 'A'..'Z') {
                count++
            }
        }
    }
    println(count)
    println(s)
}