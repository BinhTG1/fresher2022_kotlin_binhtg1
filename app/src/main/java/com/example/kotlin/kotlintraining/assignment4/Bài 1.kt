package com.example.kotlin.kotlintraining.assignment4

import java.util.*

fun<T> count(array: Array<T>):Int {
    var count = 0
    for (i in array.indices) {
        if(array[i] is Int) {
            if (array[i].toString().toInt() % 2 !=0) {
                count ++
            }
        }
    }
    return count
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Pls enter a number array")
    val array = Array(10) {scanner.next()}
    print(count(array))
}