package com.example.kotlin.kotlintraining.assignment2

import java.util.*

fun String.toBinaryString() : String {
    var bin = ""
    for (i in this.uppercase()) {
        when (i) {
            '0' -> bin += "0000"
            '1' -> bin += "0001"
            '2' -> bin += "0010"
            '3' -> bin += "0011"
            '4' -> bin += "0100"
            '5' -> bin += "0101"
            '6' -> bin += "0110"
            '7' -> bin += "0111"
            '8' -> bin += "1000"
            '9' -> bin += "1001"
            'A' -> bin += "1010"
            'B' -> bin += "1011"
            'C' -> bin += "1100"
            'D' -> bin += "1101"
            'E' -> bin += "1110"
            'F' -> bin += "1111"
        }
    }
    var count = 0
    for (i in bin) {
        if (i=='1') {
            break
        }
        count ++
    }
    return bin.substring(count, bin.length)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Input hex number: ")
    val a = scanner.next()
    val result = a.toBinaryString()
    print(result)
}