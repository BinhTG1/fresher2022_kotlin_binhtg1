package com.example.kotlin.kotlintraining.assignment2

import java.util.*

fun Int.toHexString() : String {
    return this.toString(16)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Input number: ")
    val a = scanner.nextInt()
    val hexStr = a.toHexString()
    print(hexStr)
}