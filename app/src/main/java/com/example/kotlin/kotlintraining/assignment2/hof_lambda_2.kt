package com.example.kotlin.kotlintraining.assignment2

import java.util.*


fun find(a:Int, b:Int, isPrime:(Int)->Boolean){
    println("Result: ")
    for(i in a..b) {
        if (isPrime(i)) {
            print("$i ")
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Input 1st number: ")
    var a = scanner.nextInt()
    println("Input 2nd number: ")
    var b = scanner.nextInt()
    if (a > b) {
        val tmp = b
        b = a
        a = tmp
    }

    find(a, b) {
        for (i in 2..it / 2) {
            if (it % i == 0) {
                return@find false
            }
        }
        true
    }


}