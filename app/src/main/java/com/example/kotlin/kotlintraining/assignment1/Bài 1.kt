package com.example.myapplication.assignment1

fun main() {
    var x = false
    var result = 5
    while (result < 200) {
        if (x) {
            if (result % 5 != 0) {
                print("$result,")
            }
            result += 7
            continue
        }
        if (result % 7 == 0) {
            x = true
        } else {
            result++
        }
    }
}