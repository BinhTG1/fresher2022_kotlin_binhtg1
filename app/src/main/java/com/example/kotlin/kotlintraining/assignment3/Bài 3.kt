package com.example.kotlin.kotlintraining.assignment3

import java.util.*
import kotlin.math.min

abstract class Customer {
    open var type: Char = ' '
    var amount: Int = 0
    var price: Double = 0.0
    var pay: Double = 0.0

    abstract fun payCal()

    open fun inputInfo() {
        val scanner = Scanner(System.`in`)
        println("Amount: ")
        amount = scanner.nextInt()
        println("price: ")
        price = scanner.nextDouble()
    }

    fun showInfo() {
        println("Type: $type | Buy amount: $amount | Price: $price | Salary: $pay")
    }
}

class CustomerA : Customer() {
    override var type: Char = 'A'

    override fun inputInfo() {
        super.inputInfo()
        payCal()
    }

    override fun payCal() {
        val base = price * amount
        pay = base + 0.1 * base
    }

}

class CustomerB : Customer() {
    override var type: Char = 'B'


    var year: Int = 0

    override fun inputInfo() {
        val scanner = Scanner(System.`in`)
        super.inputInfo()
        println("Year: ")
        year = scanner.nextInt()
        payCal()
    }

    override fun payCal() {
        val promo = min(year * 0.05, 0.5)
        val base = price * amount
        pay = base * (1 - promo) + base * 0.1
    }

}

class CustomerC : Customer() {
    override var type: Char = 'C'

    override fun inputInfo() {
        super.inputInfo()
        payCal()
    }

    override fun payCal() {
        val base = price * amount
        pay = base * 0.5 + 0.1 * base
    }

}

fun main() {
    val cList = Array<Customer?>(3) { null }
    val scanner = Scanner(System.`in`)
    var i = 0
    while (i < 3) {
        println("customer type: ")
        val x = scanner.nextLine().single()
        when (x) {
            'A' -> {
                val o = CustomerA()
                println("customer A: ")
                o.inputInfo()
                cList[i] = o
            }
            'B' -> {
                val o = CustomerB()
                println("customer B: ")
                o.inputInfo()
                cList[i] = o
            }
            'C' -> {
                val o = CustomerC()
                println("customer C: ")
                o.inputInfo()
                cList[i] = o
            }

        }
        i++
    }
    for (i in cList) {
        i?.showInfo()
    }
}

