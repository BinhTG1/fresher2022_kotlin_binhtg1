package com.example.myapplication.assignment1

import java.util.*

fun feb(y: Int) {
    if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
        print(29)
    }
    print(28)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Month:")
    val m = scanner.nextInt()
    println("Year:")
    val y = scanner.nextInt()
    print("Days in month: ")
    when (m) {
        1 -> print(31)
        2 -> feb(y)
        3 -> print(31)
        4 -> print(30)
        5 -> print(31)
        6 -> print(30)
        7 -> print(31)
        8 -> print(31)
        9 -> print(30)
        10 -> print(31)
        11 -> print(30)
        12 -> print(31)
    }
}