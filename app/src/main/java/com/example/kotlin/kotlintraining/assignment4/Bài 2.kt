package com.example.kotlin.kotlintraining.assignment4

import java.util.*

fun<T> swap(a:Int, b:Int, array:Array<T>) {

    val tmp = array[a]
    array[a] = array[b]
    array[b] = tmp

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Pls enter an array")
    val array = Array(10) {scanner.next()}
    swap(0, 5, array)
    for (i in array) {
        print ("$i, ")
    }
}