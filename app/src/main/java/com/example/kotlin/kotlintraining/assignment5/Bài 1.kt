package com.example.kotlin.kotlintraining.assignment5

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    var a: Double = 0.0
    var b: Double = 0.0
    var c: Boolean
    println("Nhập a và b")
    do {
        try {
            c = false
            a = scanner.nextDouble()
            b = scanner.nextDouble()
        } catch (e: InputMismatchException) {
            scanner.nextLine()
            println("Nhập ko hợp lệ!")
            println("Xin hãy nhập lại a và b")
            c = true
        }
    } while (c)
    try {
        if (b.compareTo(0) == 0) {
            throw ArithmeticException()
        } else {
            println(a / b)
        }
    } catch (e: ArithmeticException) {
        println("chia cho 0")
    }


}