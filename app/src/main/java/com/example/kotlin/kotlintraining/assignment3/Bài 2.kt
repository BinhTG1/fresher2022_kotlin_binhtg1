package com.example.kotlin.kotlintraining.assignment3

import java.util.*

interface Nhanvien {
    var name: String
    var dob: String
    var salary: Double

    fun inputInfo() {
        val scanner = Scanner(System.`in`)
        println("Your name: ")
        name = scanner.nextLine()
        println("Your dob: ")
        dob = scanner.nextLine()
    }

    fun salaryCal()

    fun showInfo() {
        println("Name: $name | DOB: $dob | Salary: ${salary.toBigDecimal().toPlainString()}")
    }

}

class Nhanvienvanphong(
    var workDays: Int = 0,
) : Nhanvien {
    override var name: String = ""
    override var dob: String = ""
    override var salary: Double = 0.0

    override fun inputInfo() {
        val scanner = Scanner(System.`in`)
        super.inputInfo()
        println("Your work day: ")
        workDays = scanner.nextInt()
        salaryCal()
    }

    override fun salaryCal() {
        salary = workDays * 100000.0
    }

}

class Nhanviensanxuat(var base: Double = 0.0, var Pamount: Int = 0) : Nhanvien {
    override var name: String = ""
    override var dob: String = ""
    override var salary: Double = 0.0

    override fun inputInfo() {
        val scanner = Scanner(System.`in`)
        super.inputInfo()
        println("Your base line: ")
        base = scanner.nextDouble()
        println("Your product amount: ")
        Pamount = scanner.nextInt()
        salaryCal()
    }

    override fun salaryCal() {
        salary = base + Pamount * 5000
    }

}

fun main() {
    println("A Nhanvienvanphong: ")
    val hel = Nhanvienvanphong()
    hel.inputInfo()
    hel.showInfo()

    println("A Nhanviensanxuat: ")
    val hell = Nhanviensanxuat()
    hell.inputInfo()
    hell.showInfo()

}