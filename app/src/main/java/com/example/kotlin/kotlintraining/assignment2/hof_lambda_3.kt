package com.example.kotlin.kotlintraining.assignment2

import java.util.*

fun sum(n:Int) :Int{
    return if (n == 1) {
        1
    } else {
        n + sum(n-1)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    print (sum(n))
}