package com.example.kotlin.kotlintraining.assignment2

import java.util.*

fun result(a:Int,f:(Int) -> Boolean){
    for (i in 1..a/2) {
        if(f(i) && f(a-i)) {
            println("$a = ${a-i} + $i")
        }
    }

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Input number: ")
    val a = scanner.nextInt()

    result(a) { c:Int ->
        for (i in 2..c / 2) {
            if (c % i == 0) {
                return@result false
            }
        }
        true
    }
}