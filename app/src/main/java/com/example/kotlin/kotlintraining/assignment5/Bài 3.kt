package com.example.kotlin.kotlintraining.assignment5

fun main() {
    val array = Array(5) { 0 }
    try {
        print(array[7])
    } catch (e: ArrayIndexOutOfBoundsException) {
        println("Không thể lấy ra phần tử ở vị trí đó")
    }
}