package com.example.kotlin.kotlintraining.thi

class Stack<T> {
    var list : List<T> = ArrayList()

    fun pop(): T? {
        return if(getSize() > 0) {
            val a = list[0]
            list = list.drop(1)
            a
        } else {
            null
        }
    }

    fun peek(): T? {
        return if(getSize()>0){
            list[0]
        } else {
            null
        }
    }

    fun isEmpty(): Boolean {
        return getSize() == 0
    }

    fun getSize(): Int {
        return list.size
    }

    fun put(input:T) {
        list = listOf(input).plus(list)
    }
}


fun main() {
    val stack = Stack<Int>()
    println("-----------------------")
    println(stack.getSize())
    println(stack.peek())
    println(stack.pop())
    println(stack.isEmpty())
    println("-----------------------")
    stack.put(1)
    stack.put(2)
    println(stack.getSize())
    println(stack.peek())
    println(stack.pop())
    println(stack.pop())
}