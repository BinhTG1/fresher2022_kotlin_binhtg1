package com.example.myapplication.assignment1

import java.util.*

fun binGen(n: Int) {
    var x = n
    var result = ""
    while (x > 0) {
        result += if (x % 2 != 0) '1' else '0'
        x /= 2
    }
    val rev = result.reversed()
    println("Bin: $rev")
    hexGen(rev)
}

fun hexGen(s: String) {
    val d = s.length % 4
    val result = "0".repeat(if (d == 0) 0 else 4 - d).plus(s)
    var hex = ""
    val x = result.length
    var i = 0
    while (i + 4 <= x) {
        when (result.subSequence(i, i + 4)) {
            "0000" -> hex += '0'
            "0001" -> hex += '1'
            "0010" -> hex += '2'
            "0011" -> hex += '3'
            "0100" -> hex += '4'
            "0101" -> hex += '5'
            "0110" -> hex += '6'
            "0111" -> hex += '7'
            "1000" -> hex += '8'
            "1001" -> hex += '9'
            "1010" -> hex += 'A'
            "1011" -> hex += 'B'
            "1100" -> hex += 'C'
            "1101" -> hex += 'D'
            "1110" -> hex += 'E'
            "1111" -> hex += 'F'
        }
        i += 4
    }
    println("Hex: $hex")
}

fun main() {
    val scanner = Scanner(System.`in`)
    print("Your input: ")
    val n: Int = scanner.nextInt()
    binGen(n)
}