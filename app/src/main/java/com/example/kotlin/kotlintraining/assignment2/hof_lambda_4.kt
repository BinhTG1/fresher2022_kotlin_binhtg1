package com.example.kotlin.kotlintraining.assignment2

import java.util.*

fun summ(n:Int, s:String) :Int{
    return if (n == 0) {
        s[n].digitToInt()
    } else {
        s[n].digitToInt() + summ(n-1, s)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val s = n.toString()
    print (summ(s.length-1, s))
}