package com.example.kotlin.kotlintraining.coroutine

import kotlinx.coroutines.*

// là cách quản lý nhiều couroutine, có thể cancel tất cả coroutine trong cùng 1 scope, là cách để ắn nhiều coroutin vào vòng đời cụ thể để tránh tràn bộ nhớ
// context là cấu hình cho Coroutine gồm job + dispatcher + name

fun main(){
    val scope2 = CoroutineScope( CoroutineName("Second"))
    scope2.launch {
        val scope = CoroutineScope( CoroutineName("First"))

        val job1 = scope.launch {
            launch {
                delay(2000L)
                println("World 2")
            }
            launch {
                delay(1000L)
                println("World 1")
            }
            println("Hello")

            val job3 = launch {
                while(isActive) {
                    delay(500L)
                    println("working...")
                }
            }

            delay(3000L)
            job3.cancel()

        }
        delay(5000L)
        job1.cancel()
        println("canceling ...")
        print (job1.isActive)
    }
    println("hello world")
    Thread.sleep(10000)
}