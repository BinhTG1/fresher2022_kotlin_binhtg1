package com.example.myapplication.assignment1

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Array length: ")
    val n: Int = scanner.nextInt()
    println("Enter your array: ")
    val a = Array(n) { scanner.nextInt() }
    a.sort()
    for (i in a) {
        print("$i ")
    }
}