package com.example.kotlin.kotlintraining.assignment5

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Nhập số lượng phần tử trong mảng: ")
    val n = scanner.nextInt()
    val array = Array<Int>(n) { 0 }
    println("Nhập mảng: ")
    for (i in 0 until n) {
        try {
            array[i] = scanner.nextInt()
            if (array[i] == 100) {
                throw Exception()
            }
        } catch (e: Exception) {
            for (j in 0 until i) {
                print("${array[j]}, ")
            }
            break
        }

    }
}