package com.example.kotlin.kotlintraining.assignment5

fun main() {
    var colors = arrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j')
    var colors2 = arrayOf('k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't')

    var list = mutableListOf<Char>(*colors)
    var list2 = mutableListOf<Char>(*colors2)
    list.addAll(list2)
    list2.clear()
    list = list.map { it.uppercaseChar() } as MutableList<Char>
    println(list)
    for (i in 0..3) {
        list.removeAt(3)
    }
    println(list)
    list.reverse()
    println(list)
}