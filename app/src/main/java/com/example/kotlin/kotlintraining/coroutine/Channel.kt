package com.example.kotlin.kotlintraining.coroutine

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*

fun main(){
//    val channel = Channel<Int>()
//    val job = launch {
//        for (x in 1..5) {
//            channel.send(x * x)
//
//        }
//        channel.close()
//    }
//
//    for (x in channel) {
//        println(x)
//    }
    val scope = CoroutineScope(CoroutineName("channel"))
    scope.launch {
        val rec : ReceiveChannel<Int> = produce {
            for (i in 1..5) {
                send(i*i)
            }
        }
        rec.consumeEach { println(it) }
    }
    Thread.sleep(20000)

}