package com.example.kotlin.kotlintraining.coroutine

import kotlinx.coroutines.*

import kotlinx.coroutines.flow.*


fun main() {
    val flow1 = flow {
        var start = 10
        while (0 < start) {
            emit(start)

            delay(1000L)

            start--
        }
    }
    val scope = CoroutineScope(CoroutineName("flow"))
    scope.launch {
//        flow1.collect {
//            i ->
//            delay(1500L)
//            println(i)
//        }
//        val count = flow1
//            .filter{
//                i -> i % 2 == 0
//            }
//            .map{
//                i -> i*i
//            }
//            .onEach{
//                print("Now $it ")
//            }
//            .count{
//                i -> i % 3 ==0
//            }

//        val reduce = flow1
//            .reduce{
//            accumulator, value -> accumulator + value
//        }

//        val fold = flow1
//            .fold(100){
//                    accumulator, value ->
//                println(accumulator)
//                accumulator + value
//            }

        flow1.flatMapConcat{
            i -> flowOf("$i abc", "def $i").transform {
                i -> emit("$i this is transform")
        }
        }.collect{
            i-> println(i)
        }
//        println(count)
//        println(reduce)
//        println(fold)
    }
    Thread.sleep(50000L)
}