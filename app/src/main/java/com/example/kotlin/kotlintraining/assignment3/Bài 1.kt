package com.example.kotlin.kotlintraining.assignment3

import java.util.*

open class Person(
    _name: String,
    _gender: Char,
    _dob: String,
    _address: String
) {
    var name: String = _name
    var gender: Char = _gender
    var dob: String = _dob
    var address: String = _address


    constructor() : this("", ' ', "", "")

    open fun inputInfo() {
        val scanner = Scanner(System.`in`)
        println("Your name: ")
        name = scanner.nextLine()
        do {
            println("Your gender: ")
            gender = scanner.nextLine().single()
        } while (gender != 'm' && gender != 'f')
        println("Your DOB: ")
        dob = scanner.nextLine()
        println("Your address: ")
        address = scanner.nextLine()
    }

    open fun showInfo() {
        println("Name: $name | Gender: $gender | DOB: $dob | Address: $address")
    }

}

class Student(
    var id: String = "SV",
    var email: String = "",
    var avgGrade: Double = 0.0,
) : Person() {
    override fun inputInfo() {
        val scanner = Scanner(System.`in`)
        super.inputInfo()
        do {
            println("Your ID with prefix (vd: SV01): ")
            id = scanner.nextLine()
        } while (if (id.length < 3) true else id.subSequence(0, 2) != "SV")
        do {
            println("Your mail (no space and have @): ")
            email = scanner.nextLine()
        } while (!email.contains('@') || email.contains(' '))
        avgGrade = scanner.nextDouble()
    }

    override fun showInfo() {
        println("Name: $name | Gender: $gender | DOB: $dob | Address: $address | ID: $id | Email: $email | AvgGrade: $avgGrade")
    }
}

class Teacher(
    var wage: Double = 0.0,
    var room: String = "",
    var hour: Double = 0.0,
) : Person() {
    override fun inputInfo() {
        val scanner = Scanner(System.`in`)
        super.inputInfo()
        do {
            println("Your class: ")
            room = scanner.nextLine()
        } while (if (room.length < 2) true else when (room[0]) {
                'G', 'H', 'I', 'K', 'L', 'M' -> false
                else -> true
            }
        )

        println("Your wage/h: ")
        wage = scanner.nextDouble()
        println("Your working hour: ")
        hour = scanner.nextDouble()
    }

    override fun showInfo() {
        println("Name: $name | Gender: $gender | DOB: $dob | Address: $address | Class: $room | Wage/h: $wage | Hours: $hour | Salary: ${salary()}")
    }

    fun salary(): Double {
        return if (room.length > 1) when (room[0]) {
            'G', 'H', 'I', 'K' -> wage * hour
            'L', 'M' -> wage * hour + 500000
            else -> 0.0
        } else 0.0
    }
}

fun main() {
    println("A person: ")
    val hel = Person()
    hel.inputInfo()
    hel.showInfo()

    println("A student: ")
    val hell = Student()
    hell.inputInfo()
    hell.showInfo()

    println("A teacher: ")
    val hello = Teacher()
    hello.inputInfo()
    hello.showInfo()
}