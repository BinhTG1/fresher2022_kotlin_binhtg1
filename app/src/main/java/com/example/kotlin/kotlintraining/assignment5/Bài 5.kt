package com.example.kotlin.kotlintraining.assignment5

import java.util.*

class Staff(
    var name: String = "",
    var gender: String = "",
    var dob: String = "",
    var trinhDo: String = "",
    var phoneNumber: String = "",
    var code: String = ""
) {
    fun add() {
        val scanner = Scanner(System.`in`)
        println("Tên: ")
        name = scanner.nextLine()

        do {
            println("Giới tính: ")
            gender = scanner.nextLine()
        } while (gender != "nam" && gender != "nu")

        do {
            println("Ngày tháng năm sinh (dd/mm/yyyy): ")
            dob = scanner.nextLine()
        } while (!dob.matches(Regex("\\d{2}/\\d{2}/\\d{4}")))

        do {
            println("Trinh độ chuyên môn (Trung cap | Cao dang | Dai hoc) (optional): ")
            trinhDo = scanner.nextLine()
        } while (when (trinhDo) {
                "Trung cap", "Cao dang", "Dai hoc", "" -> false
                else -> true
            }
        )

        do {
            println("Số điện thoại: ")
            phoneNumber = scanner.nextLine()
        } while (!phoneNumber.matches(Regex("^[0-9]*\$")))

        if (code == "") {
            code = randomID()
        }
        println("Thành công")
    }

    private fun randomID(): String = List(16) {
        (('a'..'z') + ('A'..'Z') + ('0'..'9')).random()
    }.joinToString("")

    fun display() {
        println("Tên: $name | Giới tính: $gender | DOB: $dob | SĐT: $phoneNumber | Trinh độ chuyên môn: $trinhDo | Code: $code")
    }
}

fun main() {
    val list = mutableListOf<Staff>()
    val scanner = Scanner(System.`in`)
    println("Bắt đầu chương trình")
    while (true) {
        when (scanner.nextLine()) {
            "add" -> {
                val tmp = Staff()
                tmp.add()
                list.add(tmp)
            }
            "display" -> {
                if (list.isEmpty()) {
                    println("Not found information")
                } else {
                    list.forEach { it.display() }
                }
            }
            "update" -> run {
                println("Bạn muốn chỉnh sửa ai ?")
                val a = scanner.nextLine()
                if (list.find { it.name == a || it.code == a } != null) {
                    list.find { it.name == a || it.code == a }?.add()
                } else {
                    println("Not found information")
                }
            }
            "find" -> {
                println("Bạn muốn tìm ai ?")
                val a = scanner.nextLine()
                if (list.find { it.name == a || it.code == a } != null) {
                    list.find { it.name == a || it.code == a }?.display()
                } else {
                    println("Not found information")
                }
            }
        }
    }
}