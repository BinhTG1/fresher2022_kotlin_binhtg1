package com.example.kotlin.kotlintraining.thi

import java.util.*

class QueueLinkedList<T> {
    var list = LinkedList<T>()


    fun dequeue(): T? {
        return if (!isEmpty()) {
            list.pop()
        } else {
            null
        }
    }

    fun peek(): T? {
        return list.peek()
    }

    fun isEmpty(): Boolean {
        return getSize() == 0
    }

    fun getSize(): Int {
        return list.size
    }

    fun enqueue(input:T) {
        list.addLast(input)
    }

}

fun main() {
    val queue = QueueLinkedList<Int>()
    println("-----------------------")
    queue.enqueue(1)
    println(queue.dequeue())
    println(queue.dequeue())
    println("-----------------------")
    val queueChar = QueueLinkedList<Char>()
    queueChar.isEmpty()
    queueChar.enqueue('A')
    println(queueChar.isEmpty())
    queueChar.enqueue('B')
    queueChar.enqueue('C')
    println(queueChar.dequeue())
    println(queueChar.peek())
    println(queueChar.peek())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
}