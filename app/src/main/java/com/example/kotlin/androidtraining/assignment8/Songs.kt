package com.example.kotlin.androidtraining.assignment8

import android.media.session.MediaController
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin.R


class Songs : Fragment(), SongAdapter.OnItemClickListener {
    private lateinit var recycler_view: RecyclerView
    var songList = ArrayList<Song>()
    lateinit var comm: Communicator


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_songs, container, false)
        if(arguments!=null) {
            comm = requireActivity() as Communicator
            songList = arguments?.getSerializable("songs") as ArrayList<Song>
            val adapter = SongAdapter(songList, this)
            recycler_view = v.findViewById(R.id.songs)
            recycler_view.adapter = adapter
            recycler_view.layoutManager = LinearLayoutManager(context)
            recycler_view.setHasFixedSize(true)
        }
        return v
    }

    override fun onItemClick(position: Int) {
        comm.songPicked(songList[position], position)
    }

}