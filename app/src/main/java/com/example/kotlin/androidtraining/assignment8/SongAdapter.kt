package com.example.kotlin.androidtraining.assignment8

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin.R


class SongAdapter(private val songList: ArrayList<Song>, private val listener: Songs) : RecyclerView.Adapter<SongAdapter.SongViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.song, parent, false)
        return SongViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val currentItem = songList[position]
        holder.img.setImageResource(currentItem.img)
        holder.title.text = currentItem.title
        holder.artist.text = currentItem.artist
    }

    override fun getItemCount()= songList.size

    inner class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var img: ImageView = itemView.findViewById(R.id.song_img)
        var title: TextView = itemView.findViewById(R.id.song_title)
        var artist: TextView = itemView.findViewById(R.id.song_artist)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }

    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}