package com.example.kotlin.androidtraining.assignment5

import java.io.Serializable

class Product(var name:String, var code: Int, var des: String, var price: Float, var imgSrc:Int) : Serializable