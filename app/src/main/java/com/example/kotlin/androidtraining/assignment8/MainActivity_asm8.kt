package com.example.kotlin.androidtraining.assignment8

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.MediaController
import android.widget.MediaController.MediaPlayerControl
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlin.R

interface Communicator {
    fun songPicked(song: Song, pos: Int)
}


class MainActivity_asm8 : AppCompatActivity(), MediaPlayerControl, Communicator{

    private val songList = ArrayList<Song>()
    private lateinit var controller: MediaController
    lateinit var musicService: MusicService
    var musicBound = false
    lateinit var playIntent: Intent
    var playbackPaused = false
    var paused = false

    val musicConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as MusicService.MusicBinder
            musicService = binder.getService()
            musicService.songList = songList
            musicBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            musicBound = false
        }
    }




    private fun setController() {
        controller = MediaController(this)
        controller.setPrevNextListeners({playNext()}, {playPrev()})
        controller.setMediaPlayer(this)
        controller.setAnchorView(findViewById(R.id.songsFrag))
        controller.isEnabled = true
    }

    fun playNext() {
        musicService.playNext()
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        controller.show(0);
    }

    fun playPrev() {
        musicService.playPrev();
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        controller.show(0);
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_asm8)

        songList.add(Song("Saul", "Saul Goodman", R.drawable.img, R.raw.augh))
        songList.add(Song("Sam", "Jetstream Sam", R.drawable.img1, R.raw.augh))
        val songs = Songs()
        val args = Bundle()
        val transaction = this.supportFragmentManager.beginTransaction()
        args.putSerializable("songs", songList)
        songs.arguments = args
        transaction.replace(R.id.songsFrag, songs)
        transaction.commit()

        setController()
    }

    override fun onStart() {
        super.onStart()


        playIntent = Intent(this, MusicService::class.java)
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE)
        startService(playIntent)
    }

    override fun start() {
        musicService.player.start()
        controller.show()
    }

    override fun pause() {
        playbackPaused = true
        musicService.player.pause()
    }

    override fun getDuration(): Int {
        if(musicBound && isPlaying) {
            return musicService.player.duration
        }
        return 0
    }

    override fun getCurrentPosition(): Int {
        if(musicBound && isPlaying) {
            return musicService.player.currentPosition
        }
        return 0
    }

    override fun seekTo(p0: Int) {
        musicService.player.seekTo(p0)
    }

    override fun isPlaying(): Boolean {
        if(musicBound) {
            return musicService.player.isPlaying
        }
        return false
    }

    override fun getBufferPercentage(): Int {
        return 0
    }

    override fun canPause(): Boolean {
        return true
    }

    override fun canSeekBackward(): Boolean {
        return true
    }

    override fun canSeekForward(): Boolean {
        return true
    }

    override fun getAudioSessionId(): Int {
        return 0
    }

    override fun songPicked(song: Song, pos: Int) {
        musicService.songPosn = pos
        musicService.playSong()
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        controller.show(0)
    }

    override fun onPause() {
        super.onPause()
        paused = true
    }

    override fun onStop() {
        super.onStop()
        controller.hide()
    }

    override fun onResume() {
        super.onResume()
        if(paused){
            setController();
            paused=false;
        }
    }

    override fun onDestroy() {
        stopService(playIntent)
        super.onDestroy()
    }
}