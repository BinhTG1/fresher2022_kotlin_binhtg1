package com.example.kotlin.androidtraining.assignment5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment

import androidx.navigation.fragment.findNavController

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin.R

class Detail : Fragment() {
    lateinit var ten: TextView
    lateinit var ma: TextView
    lateinit var gia: TextView
    lateinit var mota: TextView
    lateinit var button: Button
    lateinit var anh: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_detail, container, false)
        ten = v.findViewById(R.id.ten)
        ma = v.findViewById(R.id.ma)
        gia = v.findViewById(R.id.gia)
        mota = v.findViewById(R.id.mota)
        button = v.findViewById(R.id.capnhat)
        anh = v.findViewById(R.id.anh)
        val product:Product
        if(arguments!=null) {
            product = arguments?.getSerializable("product") as Product
            var pos = requireArguments().getInt("pos", 0)
            anh.setImageResource(product.imgSrc)
            ten.append(product.name)
            ma.append(product.code.toString())
            mota.append(product.des)
            gia.append(product.price.toString().plus('$'))
            button.setOnClickListener() {
                val bundle = Bundle()
                bundle.putSerializable("product", product)
                bundle.putInt("pos", pos)
                v.findNavController().navigate(R.id.input, bundle)
            }
        }
        return v
    }

}