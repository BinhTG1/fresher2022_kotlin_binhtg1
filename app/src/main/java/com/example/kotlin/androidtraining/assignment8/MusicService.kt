package com.example.kotlin.androidtraining.assignment8

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.kotlin.R
import java.lang.Exception

class MusicService : Service(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    var player = MediaPlayer()
    var songList = ArrayList<Song>()
    var songPosn :Int = 0
    var musicBind = MusicBinder()
    var songTitle: String = ""
    val NOTIFY_ID = 1
    val CHANNEL_ID = "ForegroundService"
    val audioAttributes = AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build()

    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID, "name", NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(channel)
        }
    }

    inner class MusicBinder() : Binder() {
        fun getService(): MusicService = this@MusicService
    }

    override fun onCreate() {
        super.onCreate()
        player.setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        player.setAudioAttributes(audioAttributes)

        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    override fun onBind(intent: Intent): IBinder {
        return musicBind
    }


    fun playSong() {
        player.reset()
        val song = songList[songPosn]
        songTitle = song.title
        try{
            player.setDataSource(applicationContext, Uri.parse("android.resource://com.example.kotlin/" + song.res))
        } catch (e : Exception) {
            Log.e("cant play", e.toString())
        }
        player.prepareAsync()
    }

    fun playPrev() {
        songPosn--
        if (songPosn < 0) {
            songPosn = songList.size - 1
        }
        playSong()
    }

    fun playNext() {
        songPosn ++
        if (songPosn >= songList.size) {
            songPosn = 0
        }
        playSong()
    }


    override fun onPrepared(p0: MediaPlayer?) {
        p0?.start()
        createNotificationChannel()
        val i = Intent(this, MainActivity_asm8::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendIntent = PendingIntent.getActivity(this, 0, i, 0)
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
        builder.setContentIntent(pendIntent).setSmallIcon(R.drawable.play).setTicker(songTitle).setOngoing(true).setContentTitle("Playing").setContentText(songTitle)
        val not = builder.build()
        startForeground(NOTIFY_ID, not)
    }

    override fun onError(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
        Log.e("MUSIC PLAYER", "Playback Error");
        p0?.reset();
        return false;
    }

    override fun onCompletion(p0: MediaPlayer?) {
        if(player.getCurrentPosition()>0){
            p0?.reset()
            playNext()
        }
    }
}