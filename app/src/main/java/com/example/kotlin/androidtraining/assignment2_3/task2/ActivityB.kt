package com.example.kotlin.androidtraining.assignment2_3.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.kotlin.R

class ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b2_3_task2)
        val result = intent.getIntExtra("A", 0) + intent.getIntExtra("B", 0)
        setResult(RESULT_OK, Intent(this, ActivityA::class.java).putExtra("result", result.toString()))
        finish()
    }
}