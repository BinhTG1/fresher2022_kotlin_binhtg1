package com.example.kotlin.androidtraining.assignment4.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.example.kotlin.R
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

class ActivityATask2 : AppCompatActivity(), ProductAdapter.OnItemClickListener {

    private lateinit var recycler_view: RecyclerView
    private val productList = ArrayList<Product>()
    private val adapter = ProductAdapter(productList, this)
    private var update = false
    private var pos = 0
    private lateinit var finish: ActivityResultLauncher<Intent>
    lateinit var code: EditText
    lateinit var name: EditText
    lateinit var price: EditText
    lateinit var des: EditText
    lateinit var button: Button
    lateinit var empty:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a_asm4_task2)
        code = findViewById(R.id.code_asm5)
        name = findViewById(R.id.name_asm5)
        price = findViewById(R.id.price_asm5)
        des = findViewById(R.id.des_asm5)
        button = findViewById(R.id.cf_asm5)
        empty = findViewById(R.id.empty)
        recycler_view = findViewById(R.id.recycler_view)
        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)




        finish = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode == RESULT_OK) {
                val result = it.data
                if (result != null) {

                    val product = result.getSerializableExtra("product") as? Product
                    if (product != null) {
                        code.setText(product.code.toString())
                        name.setText(product.name)
                        price.setText(product.price.toString())
                        des.setText(product.des)
                        update = true
                        pos = result.getIntExtra("pos", 0)
                        recycler_view.smoothScrollToPosition(pos)
                    }
                }
            }
        }

        if (productList.isEmpty()) {
            recycler_view.visibility = View.GONE
            empty.visibility = View.VISIBLE;
        }

        button.setOnClickListener() {
            if(!update) {
                insert()
            } else {
                productList[pos].code = code.text.toString().toInt()
                productList[pos].name = name.text.toString()
                productList[pos].des = des.text.toString()
                productList[pos].price = price.text.toString().toFloat()
                adapter.notifyItemChanged(pos)
                update = false
            }
            code.text.clear()
            name.text.clear()
            des.text.clear()
            price.text.clear()
        }


    }

    private fun insert() {
        if(isUnique(code.text.toString().toInt())) {
            productList.add(0, Product(name.text.toString(), code.text.toString().toInt(), des.text.toString(), price.text.toString().toFloat(), randomImg()))
            adapter.notifyItemInserted(0)


            if(productList.isNotEmpty()) {
                recycler_view.visibility = View.VISIBLE
                empty.visibility = View.GONE
            }
        }
        Log.d("info", "the code is not unique")
    }

    private fun isUnique(n:Int):Boolean {
        if(productList.isEmpty()) {
            return true
        }
        for (i in productList) {
            if(i.code == n) {
                return false
            }
        }
        return true
    }



    override fun onItemClick(position: Int) {
        val intent = Intent(this, ActivityBTask2::class.java)
        val clickedItem = productList[position]
        intent.putExtra("product", clickedItem)
        intent.putExtra("pos", position)
        adapter.notifyItemChanged(position)
        finish.launch(intent)
    }

//    private fun generateDummyList(size: Int): ArrayList<Product> {
//
//        val list = ArrayList<Product>()
//
//        for (i in 0 until size) {
//            val drawable = when (i % 3) {
//                0 -> R.drawable.img1
//                1 -> R.drawable.ham1
//                else -> R.drawable.notham
//            }
//
//            val item = Product("Item $i", "Item $i code", (1..500).map { i -> kotlin.random.Random.nextInt(0, charPool.size) }.map(charPool::get).joinToString(""), "30$", drawable)
//            list += item
//        }
//
//        return list
//    }

    private fun randomImg() : Int {
        val drawable = when ((1..3).random() % 3) {
            0 -> R.drawable.img1
            1 -> R.drawable.ham1
            else -> R.drawable.notham
        }
        return drawable
    }
}