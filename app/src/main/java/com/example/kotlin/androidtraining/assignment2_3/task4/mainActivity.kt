package com.example.kotlin.androidtraining.assignment2_3.task4

import android.content.BroadcastReceiver
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.kotlin.R

class mainActivity : AppCompatActivity() {
    lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task5)
        button = findViewById(R.id.button2)


        button.setOnClickListener() {
            for(i in 1..5) {
                sendOrderedBroadcast(Intent("task5").putExtra("num", (1..5).random()), null)
            }
        }
    }
}