package com.example.kotlin.androidtraining.assignment4.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.kotlin.R
import com.example.kotlin.androidtraining.assignment4.task1.ActivityA
import java.io.Serializable

class ActivityBTask2 : AppCompatActivity() {

    lateinit var ten: TextView
    lateinit var ma: TextView
    lateinit var gia: TextView
    lateinit var mota: TextView
    lateinit var button: Button
    lateinit var anh: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b_asm4_task2)

        ten = findViewById(R.id.ten)
        ma = findViewById(R.id.ma)
        gia = findViewById(R.id.gia)
        mota = findViewById(R.id.mota)
        button = findViewById(R.id.capnhat)
        anh = findViewById(R.id.anh)

        val product = intent.getSerializableExtra("product") as? Product

        if (product != null) {
            anh.setImageResource(product.imgSrc)
            ten.append(product.name)
            ma.append(product.price.toString())
            mota.append(product.des)
            gia.append(product.price.toString().plus('$'))
        }

        button.setOnClickListener() {
            setResult(RESULT_OK, Intent(this, ActivityATask2::class.java).putExtra("product", product).putExtra("pos", intent.getIntExtra("pos", 0)))
            finish()
        }
    }
}