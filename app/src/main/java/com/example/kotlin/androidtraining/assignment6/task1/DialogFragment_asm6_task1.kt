package com.example.kotlin.androidtraining.assignment6.task1

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.example.kotlin.R



class DialogFragment_asm6_task1 : DialogFragment() {
    lateinit var yes: Button
    lateinit var no: Button

    interface Listener {
        fun sendInput(str:String)
    }

    lateinit var listener:Listener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_asm6_task1, container, false)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        yes = view.findViewById(R.id.yes)
//        no = view.findViewById(R.id.no)
//
//        yes.setOnClickListener() {
//            listener.sendInput("You click yes")
//            dismiss()
//        }
//
//        no.setOnClickListener() {
//            listener.sendInput("You click no")
//            dismiss()
//        }
//
//    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
            .setTitle("Confirm dialog title")
            .setMessage("Confirm dialog content")
            .setPositiveButton("Yes",
                DialogInterface.OnClickListener() { _, _ -> listener.sendInput("you click yes")})
            .setNegativeButton("No",
                DialogInterface.OnClickListener() { _, _ -> listener.sendInput("you click no")})
            .create()


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = requireActivity() as Listener

    }
}