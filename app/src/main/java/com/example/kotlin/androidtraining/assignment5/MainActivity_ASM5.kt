package com.example.kotlin.androidtraining.assignment5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController

import com.example.kotlin.R


interface Communicator {
    fun passDataCom(inputProduct: Product, pos: Int)
}

class MainActivity_ASM5 : AppCompatActivity(), Communicator {
    private val productList = ArrayList<Product>()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_asm5)

    }



    override fun passDataCom(inputProduct: Product, pos: Int) {
        val foodList = FoodList()
        val args = Bundle()
        val transaction = this.supportFragmentManager.beginTransaction()
        if (pos == -1) {
            if(isUnique(inputProduct.code)) {
                productList.add(0,
                    inputProduct
                )
            }
        } else {
            productList[pos] = inputProduct
        }
        args.putSerializable("products", productList)
        foodList.arguments = args
        transaction.replace(R.id.listFrag, foodList)
        transaction.commit()
    }



    private fun isUnique(n:Int):Boolean {
        if(productList.isEmpty()) {
            return true
        }
        for (i in productList) {
            if(i.code == n) {
                return false
            }
        }
        return true
    }
}