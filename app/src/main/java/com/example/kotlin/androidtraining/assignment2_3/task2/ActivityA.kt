package com.example.kotlin.androidtraining.assignment2_3.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import com.example.kotlin.R

class ActivityA : AppCompatActivity() {
    lateinit var editTextNumberA: EditText
    lateinit var editTextNumberB: EditText
    lateinit var sum:Button
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a2_3_task2)
        editTextNumberA = findViewById(R.id.editTextNumberA)
        editTextNumberB = findViewById(R.id.editTextNumberB)
        sum = findViewById(R.id.button)
        val getSum = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            ActivityResultCallback {
                if(it.resultCode == RESULT_OK) {
                    val result = it.data
                    if (result != null) {
                        Log.d("Sum", result.getStringExtra("result").toString())
                    }
                }
            }
        )
        sum.setOnClickListener() {
            if(editTextNumberA.text.toString().isEmpty() || editTextNumberB.text.toString().isEmpty()) {
                Log.d("Ban chua nhap so", "Moi ban nhap so!")
            } else {
                val A = editTextNumberA.text.toString().toInt()
                val B = editTextNumberB.text.toString().toInt()

                getSum.launch(Intent(this, ActivityB::class.java).putExtra("A", A).putExtra("B", B))
            }
        }
    }

}