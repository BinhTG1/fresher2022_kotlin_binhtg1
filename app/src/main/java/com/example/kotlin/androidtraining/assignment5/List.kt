package com.example.kotlin.androidtraining.assignment5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin.R

class FoodList : Fragment(), ProductAdapter.OnItemClickListener {
    private lateinit var recycler_view: RecyclerView
    private lateinit var comm: Communicator
    var products = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_list, container, false)
        comm = requireActivity() as Communicator
        if(arguments!=null) {
            products = arguments?.getSerializable("products") as ArrayList<Product>
            val adapter = ProductAdapter(products, this)
            recycler_view = v.findViewById(R.id.recycler_view)
            recycler_view.adapter = adapter
            recycler_view.layoutManager = LinearLayoutManager(context)
            recycler_view.setHasFixedSize(true)
        }
        return v

    }

    override fun onItemClick(position: Int) {
        recycler_view.smoothScrollToPosition(position)
        val args = Bundle()
        args.putSerializable("product", products[position])
        args.putInt("pos", position)
        val navController = findNavController()
        navController.navigate(R.id.detail, args)
    }
}