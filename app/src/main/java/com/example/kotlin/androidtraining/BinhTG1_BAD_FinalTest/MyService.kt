package com.example.kotlin.androidtraining.BinhTG1_BAD_FinalTest

import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlin.concurrent.thread

class MyService : JobService() {
    override fun onStartJob(p0: JobParameters?): Boolean {
        var sum = 0
        for(i in 1..1000000) {
            sum += i
        }
        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            Log.e("INTERRUPTED", e.toString())
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent("TheSum").putExtra("sum", sum))
        return false;
    }

    override fun onStopJob(p0: JobParameters?): Boolean {
        Log.d("Stop job", "Stopped")
        return false
    }

}