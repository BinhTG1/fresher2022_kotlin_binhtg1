package com.example.kotlin.androidtraining.assignment4.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.kotlin.R

class ActivityB : AppCompatActivity() {
    lateinit var taikhoan: TextView
    lateinit var hoten: TextView
    lateinit var tuoi: TextView
    lateinit var gioitinh: TextView
    lateinit var ok: Button
    lateinit var cancel: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b4)
        taikhoan = findViewById(R.id.taikhoan)
        hoten = findViewById(R.id.hoten)
        tuoi = findViewById(R.id.tuoi)
        gioitinh = findViewById(R.id.gioitinh)
        ok = findViewById(R.id.ok)
        cancel = findViewById(R.id.cancel)


        hoten.append(intent.getStringExtra("ht"))
        taikhoan.append(intent.getStringExtra("tk"))
        tuoi.append(intent.getStringExtra("t"))
        gioitinh.append(intent.getStringExtra("gt"))

        ok.setOnClickListener() {
            setResult(RESULT_OK, Intent(this, ActivityA::class.java))
            finish()
        }
        cancel.setOnClickListener() {
            var i = Intent(this, ActivityA::class.java)
            i.putExtra("tk", intent.getStringExtra("tk"))
            i.putExtra("ht", intent.getStringExtra("ht"))
            i.putExtra("t", intent.getStringExtra("t"))
            i.putExtra("gt", if(intent.getStringExtra("gt") == "Nam") 0 else 1)
            setResult(RESULT_CANCELED, i)
            finish()
        }
    }
}