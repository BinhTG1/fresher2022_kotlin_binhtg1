package com.example.kotlin.androidtraining.assignment2_3.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kotlin.R

class ActivityA : AppCompatActivity() {
    val list = arrayListOf<String>("Hello!", "Hi!", "Salut!", "Hallo!", "Ciao!","Ahoj!", "YAHsahs!", "Bog!", "Hej ! "," Czesc!"," Ní! "," Kon'nichiwa! ", " Annyeonghaseyo!", "Shalom!", "Sah-wahd-dee-kah! ", " Merhaba!", " Hujambo! ", " Olá! ")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a2_3_1)
        startActivity(Intent(this, ActivityB::class.java).putExtra("List", list))
    }


}