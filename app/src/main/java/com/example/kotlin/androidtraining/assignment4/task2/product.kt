package com.example.kotlin.androidtraining.assignment4.task2

import java.io.Serializable

class Product(var name:String, var code: Int, var des: String, var price: Float, var imgSrc:Int) : Serializable