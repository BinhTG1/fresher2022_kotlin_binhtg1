package com.example.kotlin.androidtraining.assignment4.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import com.example.kotlin.R

class ActivityA : AppCompatActivity() {
    lateinit var tk: EditText
    lateinit var ht: EditText
    lateinit var t: EditText
    lateinit var gt: Spinner
    lateinit var cf: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a4)
        tk = findViewById(R.id.tk)
        ht = findViewById(R.id.ht)
        t = findViewById(R.id.t)
        gt = findViewById(R.id.gt)
        cf = findViewById(R.id.cf)

        val gender = resources.getStringArray(R.array.gender)
        gt.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)



        val finish = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            ActivityResultCallback {
                if(it.resultCode == RESULT_CANCELED) {
                    val result = it.data
                    if (result != null) {
                        tk.setText("${result.getStringExtra("tk")}")
                        ht.setText("${result.getStringExtra("ht")}")
                        t.setText("${result.getStringExtra("t")}")
                        gt.setSelection(result.getIntExtra("gt", 0))
                    }
                }
            }
        )

        cf.setOnClickListener() {
            var intent = Intent(this, ActivityB::class.java)
            intent.putExtra("tk", tk.text.toString())
            intent.putExtra("ht", ht.text.toString())
            intent.putExtra("t", t.text.toString())
            intent.putExtra("gt", gt.selectedItem.toString())
            finish.launch(intent)
        }
    }
}