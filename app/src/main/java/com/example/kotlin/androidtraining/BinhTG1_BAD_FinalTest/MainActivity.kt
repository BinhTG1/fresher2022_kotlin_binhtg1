package com.example.kotlin.androidtraining.BinhTG1_BAD_FinalTest

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.kotlin.R

class MainActivity : AppCompatActivity() {
    val fragmentManager: FragmentManager = supportFragmentManager
    val Receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val dialog = Dialog()
            val sum = p1?.getIntExtra("sum", 0)
            val args = Bundle()
            if (sum != null) {
                args.putInt("sum", sum)
            }
            dialog.arguments = args
            dialog.show(fragmentManager, null)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val jobScheduler = applicationContext.getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler
        val componentName = ComponentName(this, MyService::class.java)
        val jobInfo = JobInfo.Builder(1, componentName).build()
        jobScheduler.schedule(jobInfo)

        LocalBroadcastManager.getInstance(this).registerReceiver(
            Receiver, IntentFilter("TheSum")
        )

    }
}