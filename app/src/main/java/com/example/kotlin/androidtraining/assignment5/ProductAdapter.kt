package com.example.kotlin.androidtraining.assignment5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin.R
import java.io.Serializable

class ProductAdapter(private val productList: ArrayList<Product>, private val listener: FoodList) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.product, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val currentItem = productList[position]
        holder.img.setImageResource(currentItem.imgSrc)
        holder.name.text = currentItem.name
        holder.des.text = currentItem.des
        holder.price.text = currentItem.price.toString().plus('$')
    }

    override fun getItemCount() = productList.size

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var img: ImageView = itemView.findViewById(R.id.img)
        var name: TextView = itemView.findViewById(R.id.name)
        var des: TextView = itemView.findViewById(R.id.des)
        var price: TextView = itemView.findViewById(R.id.price)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}