package com.example.kotlin.androidtraining.assignment2_3.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.kotlin.R

class ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b2_3_1)
        Log.d("The List", "List: ${intent.getStringArrayListExtra("List")}")
    }
}