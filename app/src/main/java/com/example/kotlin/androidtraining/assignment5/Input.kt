package com.example.kotlin.androidtraining.assignment5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.example.kotlin.R

class Input : Fragment() {
    lateinit var code: EditText
    lateinit var name: EditText
    lateinit var price: EditText
    lateinit var des: EditText
    lateinit var button: Button
    private lateinit var comm: Communicator
    private var update = false
    private var pos = -1


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_input, container, false)
        comm = requireActivity() as Communicator
        code = v.findViewById(R.id.code_asm5)
        name = v.findViewById(R.id.name_asm5)
        price = v.findViewById(R.id.price_asm5)
        des = v.findViewById(R.id.des_asm5)
        button = v.findViewById(R.id.cf_asm5)
        if (arguments !=null ) {
            val product = arguments?.getSerializable("product") as Product
            code.setText(product.code.toString())
            name.setText(product.name)
            price.setText(product.price.toString())
            des.setText(product.des)
            pos = requireArguments().getInt("pos")
            update = true
        }

        button.setOnClickListener() {
            if((code.text.isNotEmpty() && name.text.isNotEmpty())&&(price.text.isNotEmpty() && des.text.isNotEmpty())) {
                if (update) {
                    update = false
                    comm.passDataCom(Product(name.text.toString(), code.text.toString().toInt(), des.text.toString(), price.text.toString().toFloat(), randomImg()), pos)
                } else {
                    comm.passDataCom(Product(name.text.toString(), code.text.toString().toInt(), des.text.toString(), price.text.toString().toFloat(), randomImg()), -1)
                }
            }
            code.text.clear()
            name.text.clear()
            des.text.clear()
            price.text.clear()
        }
        return v
    }

    private fun randomImg() : Int {
        val drawable = when ((1..3).random() % 3) {
            0 -> R.drawable.img1
            1 -> R.drawable.ham1
            else -> R.drawable.notham
        }
        return drawable
    }
}