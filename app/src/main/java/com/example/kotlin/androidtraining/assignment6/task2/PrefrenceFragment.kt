package com.example.kotlin.androidtraining.assignment6.task2

import android.os.Bundle
import androidx.preference.*
import com.example.kotlin.R


class PreferenceFrag : PreferenceFragmentCompat () {


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = pref.edit()
        val mTextPreferrence = preferenceScreen.findPreference<Preference>("text") as EditTextPreference?
//        val preferenceChangeListener: Preference.OnPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue->
//            editor.putString("text", newValue.toString())
//            editor.commit()
//            false
//        }
//        mTextPreferrence?.onPreferenceChangeListener = preferenceChangeListener
        val mCheckPreferrence = preferenceScreen.findPreference<Preference>("checkbox") as CheckBoxPreference?
//        val preferenceChangeListener2: Preference.OnPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue->
//            editor.putBoolean("checkbox", newValue.toString().toBoolean())
//            editor.commit()
//            false
//        }
//        mCheckPreferrence?.onPreferenceChangeListener = preferenceChangeListener2
        val mListPreferrence = preferenceScreen.findPreference<Preference>("list") as ListPreference?
//        val preferenceChangeListener3: Preference.OnPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue->
//            editor.putString("list", newValue.toString())
//            editor.commit()
//            false
//        }
//        mListPreferrence?.onPreferenceChangeListener = preferenceChangeListener3
        val mSwitchPreference = preferenceScreen.findPreference<Preference>("switch") as SwitchPreference?
//        val preferenceChangeListener4: Preference.OnPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue->
//            editor.putBoolean("switch", newValue.toString().toBoolean())
//            editor.commit()
//            false
//        }
//        mSwitchPreference?.onPreferenceChangeListener = preferenceChangeListener4
        val mSeekPreferrence = preferenceScreen.findPreference<Preference>("seek") as SeekBarPreference?
//        val preferenceChangeListener5: Preference.OnPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue->
//            editor.putString("seek", newValue.toString())
//            editor.commit()
//            false
//        }
//        mSeekPreferrence?.onPreferenceChangeListener = preferenceChangeListener5
        }
    }


