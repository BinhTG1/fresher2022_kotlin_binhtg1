package com.example.kotlin.androidtraining.assignment2_3.task4

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyReceiver5: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val result = getResultExtras(true)
        val num = result.getInt("num")
        Log.d("Receiver5", num.toString())
    }
}