package com.example.kotlin.androidtraining.assignment2_3.task3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.kotlin.R

class appA : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)
        print(applicationContext.packageName)
        Log.d("name", applicationContext.packageName)
    }
}