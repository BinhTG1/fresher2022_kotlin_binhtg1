package com.example.kotlin.androidtraining.assignment6.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.preference.PreferenceManager
import com.example.kotlin.R


class MainActivity_asm6_task2 : AppCompatActivity() {

    lateinit var setting: Button
    lateinit var check: TextView
    lateinit var edit: TextView
    lateinit var list: TextView
    lateinit var switch: TextView
    lateinit var seek: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_asm6_task2)
        setting=findViewById(R.id.setting)
        check = findViewById(R.id.check)
        edit = findViewById(R.id.textpref)
        list = findViewById(R.id.listpref)
        switch = findViewById(R.id.switchpref)
        seek = findViewById(R.id.seekpref)

        setting.setOnClickListener() {
            startActivity(Intent(this, SettingActivity_asm6_task2::class.java))
        }
        check.append(pref.getBoolean("checkbox", true).toString())
        edit.append(pref.getString("text", "unset"))
        list.append(pref.getString("List", "unset"))
        switch.append(pref.getBoolean("Switch", true).toString())
        seek.append(pref.getInt("Seek", 0).toString())

    }
}