package com.example.kotlin.androidtraining.assignment6.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import com.example.kotlin.R

class SettingActivity_asm6_task2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_asm6_task2)
        val fragmentManager: FragmentManager = supportFragmentManager
        val pref = PreferenceFrag()
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.container, pref)
        fragmentTransaction.commit()
    }
}