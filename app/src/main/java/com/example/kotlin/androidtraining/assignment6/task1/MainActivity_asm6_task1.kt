package com.example.kotlin.androidtraining.assignment6.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.example.kotlin.R

class MainActivity_asm6_task1 : AppCompatActivity(), DialogFragment_asm6_task1.Listener {

    lateinit var tv: TextView
    lateinit var btn: Button
    val fragmentManager: FragmentManager = supportFragmentManager
    val dialogFragment_asm6_task1 = DialogFragment_asm6_task1()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_asm6_task1)
        tv = findViewById(R.id.tv_asm6_task1)
        btn = findViewById(R.id.cf_asm6_task1)
        btn.setOnClickListener() {
            dialogFragment_asm6_task1.show(fragmentManager, null)
        }
    }

    override fun sendInput(str: String) {
        tv.text = str
    }
}