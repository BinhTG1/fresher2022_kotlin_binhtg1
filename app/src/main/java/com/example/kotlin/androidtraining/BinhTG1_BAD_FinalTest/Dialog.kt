package com.example.kotlin.androidtraining.BinhTG1_BAD_FinalTest

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class Dialog : DialogFragment() {
    var sum = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        sum = requireArguments().getInt("sum")
        return AlertDialog.Builder(activity)
            .setTitle("Sum from 1 to 1000000")
            .setMessage(sum.toString())
            .create()
    }
}